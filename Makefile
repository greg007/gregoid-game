#
# Jiri Bilek
# 4.2.2013
#

PROJ = gregoid
SOURC= gregoid.c
CFLAGS=-std=c99
CC = gcc

all:
	$(CC) $(CFLAGS) $(SOURC) -o $(PROJ)

run:
	./$(PROJ)

clean:
	rm -f *.o $(PROJ)

