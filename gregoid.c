#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define VYBUCH 5

typedef struct{
    int x;
    int y;
    int e; // existuje
}poloha;


/*
    vykresli hraci pole se vsim vsudy
*/
void vykresli(int velikost_pole, int pole[20][20][2], poloha hrac, poloha bomb, poloha *bubak, int pocet_bubaku){

    // kopie
    int pole2[20][20][2];
    for (int x = 0 ; x < velikost_pole ; x++ )
        for (int y = 0 ; y < velikost_pole ; y++ )
            for (int s = 0 ; s < 2 ; s++ )
                pole2[x][y][s]=pole[x][y][s];
    
    // horni ohraniceni
    for (int i = 0 ; i < velikost_pole ; i++ )
        printf(" __");
    printf("\n");
    
    // vlozeni bubaku do pole
    for (int i = 0 ; i < pocet_bubaku ; i++ )
    {
        if (bubak[i].e == 1)
            if (pole[bubak[i].x][bubak[i].y][1]==0 || pole[bubak[i].x][bubak[i].y][1]==2)
                pole[bubak[i].x][bubak[i].y][1] = 2;
            else
                pole[bubak[i].x][bubak[i].y][1] = 3;
    }
    
    // vykresleni pole
    for (int i = 0 ; i < velikost_pole ; i++ )
    {
        for (int y = 0 ; y < velikost_pole ; y++ )
        {    
            if (pole[i][y][0])
                printf("|");
            else
                printf(" ");

            if (pole[i][y][1] == 1)
            {
                if (hrac.x == i && hrac.y == y)
                    printf("*_");
                else if (bomb.e == 1 && bomb.x == i && bomb.y == y)
                    printf("%c_",3);
                else
                    printf("__");
            }
            else if (pole[i][y][1] == 0)
            {
                if (hrac.x == i && hrac.y == y)
                    printf("* ");
                else if (bomb.e == 1 && bomb.x == i && bomb.y == y)
                    printf("%c ",3);
                else
                    printf("  ");
            }
            else if (pole[i][y][1] == 2)
                if (i == hrac.x && y == hrac.y)
                    printf("+ ");
                else
                    printf("O ");
            else if (pole[i][y][1] == 3)
                if (i == hrac.x && y == hrac.y)
                    printf("+_");
                else
                    printf("O_");
        }    
        if (i!=0)
            printf("|\n");
        else // vychod
            printf(" \n");
    }

    // navraceni kopie    
    for (int x = 0 ; x < velikost_pole ; x++ )
        for (int y = 0 ; y < velikost_pole ; y++ )
            for (int s = 0 ; s < 2 ; s++ )
                pole[x][y][s]=pole2[x][y][s];
}

/*
    vygeneruje hraci pole
*/
void vytvor(int velikost_pole, int pole[20][20][2]){
    for (int i = 0 ; i < velikost_pole ; i++ )
    {
        for (int y = 0 ; y < velikost_pole ; y++ )
        {    
            if (rand()%2 || y==0)
                pole[i][y][0] = 1;
            else
                pole[i][y][0] = 0;

            if (rand()%2 || i==velikost_pole-1 )
                pole[i][y][1] = 1;
            else
                pole[i][y][1] = 0;
        }
    }
    //vchod
    pole[velikost_pole-1][0][0]=0;
    
    // aby nebyl na jednom ctverecku a neslo se dal hnout
    // uvolnim mu dva ctverecky vpravo a dva nahore
    pole[velikost_pole-1][1][0] = 0; // |
    pole[velikost_pole-1][2][0] = 0;
    pole[velikost_pole-2][0][1] = 0; // __
    pole[velikost_pole-3][0][1] = 0;
}


/*
    kdyz bouchne bomba
*/
void bum(poloha * bomb, poloha *bubak, int pocet_bubaku, poloha * hrac, int pole[20][20][2], int velikost_pole,int pocet_zabitych_bubaku, int pocet_tahu, int pocet_bomb){
    // smaze okolni steny
    if (bomb->y > 0)
        pole[bomb->x][bomb->y][0] = 0;
    if (bomb->y < velikost_pole-1)
        pole[bomb->x][bomb->y+1][0] = 0;
    if (bomb->x < velikost_pole-1)
        pole[bomb->x][bomb->y][1] = 0;
    if (bomb->x > 0)
        pole[bomb->x-1][bomb->y][1] = 0;
    
    bomb->e = 0; // bomba se uz nevykresluje

        // pomaze okolni bubaky
    for (int i = 0 ; i < pocet_bubaku ; i++ )
        if (
            (bubak[i].x==bomb->x-1 && bubak[i].y==bomb->y)   ||
            (bubak[i].x==bomb->x   && bubak[i].y==bomb->y-1) ||
            (bubak[i].x==bomb->x+1 && bubak[i].y==bomb->y)   ||
            (bubak[i].x==bomb->x   && bubak[i].y==bomb->y+1))
                bubak[i].e = 0;
                
    printf("BOOOOOOM !!!\n");
    
    // jestli u bomby hrac stoji, tak to ma za sebou
    if ((hrac->x==bomb->x && hrac->y==bomb->y) ||
        (hrac->x==bomb->x-1 && hrac->y==bomb->y) ||
        (hrac->x==bomb->x && hrac->y==bomb->y-1) ||
        (hrac->x==bomb->x+1 && hrac->y==bomb->y) ||
        (hrac->x==bomb->x && hrac->y==bomb->y+1))
        {
            printf("  YOU ARE DEAD !! \n");
        // spocita pocet mrtvych bubaku
        for (int i = 0 ; i < pocet_bubaku ; i++ )
            if (bubak[i].e == 0) // neni, tedy je mrtvy
                pocet_zabitych_bubaku++;
    
        printf("Statistics: \n\tturns: %d\n\tbombs: %d\n\tdead zombies: %d\n",pocet_tahu,pocet_bomb,pocet_zabitych_bubaku);
            exit(0);
        }
}

/*
    inteligence bubaku
*/
void i_bubak(poloha * bubak, int pole[20][20][2], int velikost_pole, poloha hrac, poloha bomb){
    // metoda inteligence :
    int smer;
        // nahodne vygeneruje tah
        //smer = rand ()%4;

        // postupne vybira smery a vyhledava cil, pokud najde, jde tim smerem
        
        poloha hledac;
        
        for (smer = 0 ; smer < 4 ; smer++)
        {
            // jestli vubec jde tim smerem jit
            if (smer==0) // nahoru
                if (!(pole[bubak->x-1][bubak->y][1]==0 && bubak->x > 0
            && (bomb.e==0 || !(bomb.x==bubak->x-1 && bomb.y==bubak->y))))
                continue;
                
            if (smer==1) // vpravo
                if (!(pole[bubak->x][bubak->y+1][0]==0 && bubak->y < velikost_pole-1
            && (bomb.e==0 || !(bomb.x==bubak->x && bomb.y==bubak->y+1))))
                continue;
                
            if (smer==2) // dolu
                if (!(pole[bubak->x][bubak->y][1]==0 && bubak->x < velikost_pole-1
            && (bomb.e==0 || !(bomb.x==bubak->x+1 && bomb.y==bubak->y))))
                    continue;
                    
            if (smer==3) // vlevo
                if (!(pole[bubak->x][bubak->y][0]==0 && bubak->y > 0
            && (bomb.e==0 || !(bomb.x==bubak->x && bomb.y==bubak->y-1))))
                    continue;

            
            // pozice polohy se nastavi na novou polohu
            switch (smer){
                case 0: // nahoru
                        hledac.x=bubak->x-1;
                        hledac.y=bubak->y;
                    break;
                case 1: // vpravo
                        hledac.y=bubak->y+1;
                        hledac.x=bubak->x;
                    break;
                case 2: // dolu
                        hledac.x=bubak->x+1;
                        hledac.y=bubak->y;
                    break;
                case 3: // vlevo
                        hledac.y=bubak->y-1;
                        hledac.x=bubak->x;
                    break;
            }
            
            // kontrola, zda nebylo nalezeno
            if (hrac.x == hledac.x && hrac.y == hledac.y)
                break;
            
            int nasel = 0; // 0 - nenasel ; 1 - nasel
            int smer2 = smer;
            int counter = 2000; // abychom se nezacyklili
            while(hledac.x != bubak->x || hledac.y != bubak->y) // dokud nenarazi zase sam na sebe
            {
                counter--;
                if (counter<=0) break;
//            printf("hledac=%d:%d\n",hledac.x,hledac.y);
                // podle posledniho pohybu se zkusi posunout prvne vlevo a pak rovne a pak vpravo,
                // pokud nic, tak zpet
                int sm = smer2;
                int sm2= smer2;
                for (int sm2 = sm+3 ; sm2 < sm+3+4 ; sm2++ )
                {
                    smer2 = sm2%4;
                    // jestli vubec jde tim smerem jit
                    if (smer2==0) // nahoru
                        if (!(pole[hledac.x-1][hledac.y][1]==0 && hledac.x > 0
                    && (bomb.e==0 || !(bomb.x==hledac.x-1 && bomb.y==hledac.y))))
                        continue;
                        
                    if (smer2==1) // vpravo
                        if (!(pole[hledac.x][hledac.y+1][0]==0 && hledac.y < velikost_pole-1
                    && (bomb.e==0 || !(bomb.x==hledac.x && bomb.y==hledac.y+1))))
                        continue;
                        
                    if (smer2==2) // dolu
                        if (!(pole[hledac.x][hledac.y][1]==0 && hledac.x < velikost_pole-1
                    && (bomb.e==0 || !(bomb.x==hledac.x+1 && bomb.y==hledac.y))))
                            continue;
                            
                    if (smer2==3) // vlevo
                        if (!(pole[hledac.x][hledac.y][0]==0 && hledac.y > 0
                    && (bomb.e==0 || !(bomb.x==hledac.x && bomb.y==hledac.y-1))))
                            continue;

                    
                    // pozice polohy se nastavi na novou polohu
                    switch (smer2){
                        case 0: // nahoru
                                hledac.x=hledac.x-1;
                            break;
                        case 1: // vpravo
                                hledac.y=hledac.y+1;
                            break;
                        case 2: // dolu
                                hledac.x=hledac.x+1;
                            break;
                        case 3: // vlevo
                                hledac.y=hledac.y-1;
                            break;
                    }
                    break;
                }
                if (hrac.x == hledac.x && hrac.y == hledac.y)
                {
                    nasel = 1;
                    break;
                }
            }
            // jestlize nalezl cestu -> break;
            if (nasel) break;
        }
        
        
//            printf("%d\n",smer);
    if (smer == 4)
        smer = rand ()%4;
        
    // rozdilne hodnoty pri vyhledavani, proto se to prepise
    switch(smer){
        case 0: smer=0;break;
        case 1: smer=3;break;
        case 2: smer=1;break;
        case 3: smer=2;break;
    }
    
    
    // podle smeru posouvame bubaka
    switch (smer){
        case 0: // nahoru
            if (pole[bubak->x-1][bubak->y][1]==0 && bubak->x > 0
            && (bomb.e==0 || !(bomb.x==bubak->x-1 && bomb.y==bubak->y)))
                bubak->x-=1;
            break;
        case 1: // dolu
            if (pole[bubak->x][bubak->y][1]==0 && bubak->x < velikost_pole-1
            && (bomb.e==0 || !(bomb.x==bubak->x+1 && bomb.y==bubak->y)))
                bubak->x+=1;
            break;
        case 2: // vlevo
            if (pole[bubak->x][bubak->y][0]==0 && bubak->y > 0
            && (bomb.e==0 || !(bomb.x==bubak->x && bomb.y==bubak->y-1)))
                bubak->y-=1;
            break;
        case 3: // vpravo
            if (pole[bubak->x][bubak->y+1][0]==0 && bubak->y < velikost_pole-1
            && (bomb.e==0 || !(bomb.x==bubak->x && bomb.y==bubak->y+1)))
                bubak->y+=1;
            break;
    }
}


/////////////////////////////////////////////////
///////////// MAIN //////////////////////////////
/////////////////////////////////////////////////

int main(int argc,char *argv[]){

int velikost_pole = 20;
int pocet_bubaku = 20;
srand(time(NULL));
poloha bomb;
bomb.e = 0; // 0 neexistuje - 1 je
int tbomb = 0; // cas do vybuchu

if (argc == 2)
    pocet_bubaku = atoi(argv[1]);
/*   
if (argc == 3)
{
    velikost_pole = atoi(argv[1]);
    pocet_bubaku = atoi(argv[2]);
}
*/

// nastaveni pocatecni polohy hrace
poloha hrac = {velikost_pole-1,0};
int pole[velikost_pole][velikost_pole][2];
poloha bubak[pocet_bubaku];

// inicializace bubaku
for (int i = 0 ; i < pocet_bubaku ; i++ )
{
    bubak[i].x = rand()%(velikost_pole-1);
    bubak[i].y = 1+rand()%(velikost_pole-1);
    bubak[i].e = 1;
}

// vytvoreni pole
vytvor(velikost_pole,pole);

/// vykresleni pole
vykresli(velikost_pole,pole,hrac,bomb,bubak,pocet_bubaku);

int pocet_tahu = 0;
int pocet_bomb = 0;
int pocet_zabitych_bubaku = 0;

while (1){
    int p;
    p=getchar();

    switch (p){
    case 'h': printf("Napoveda:\n\th-napoveda\n\t8(w)-nahoru\n\t2(s)-dolu\n\t4(a)-doleva\n\t6(d)-doprava\n\t5(b)-polozit bombu\n\tx-exit\n\n");break;

    case '8':
    case 'w': if (hrac.x > 0 && pole[hrac.x-1][hrac.y][1]==0)
                hrac.x-=1;
              break;
    case 's':
    case '2': if (hrac.x < velikost_pole-1 && pole[hrac.x][hrac.y][1]==0)
                hrac.x+=1;
              break;
    case 'd':
    case '6': if (hrac.y < velikost_pole-1 && pole[hrac.x][hrac.y+1][0]==0)
                hrac.y+=1;
              break;
    case 'a':
    case '4': if (hrac.y > 0 && pole[hrac.x][hrac.y][0]==0)
                hrac.y-=1;
              break;
    case 'b':
    case '5': 
            if (bomb.e == 0){
              bomb.e = 1;
              tbomb = VYBUCH;
              bomb.x=hrac.x;
              bomb.y=hrac.y;
              pocet_bomb++;
            }
              break;
    case '0': vykresli(velikost_pole,pole,hrac,bomb,bubak,pocet_bubaku);
              break;
    case 'x': exit(0);
    }
    
    // pohyb bubaku
    if (p=='8' || p=='2' || p=='4' || p=='6' || p=='5' ||
        p=='w' || p=='s' || p=='a' || p=='d' || p=='b')
    {
        if (bomb.e == 1)// odpocet casu do vybuchu
            tbomb -= 1;
            
        pocet_tahu++; // pripocte pocet tahu
        for (int i = 0 ; i < pocet_bubaku ; i++ )
            i_bubak(&bubak[i],pole,velikost_pole,hrac,bomb);
    }     

    if (bomb.e == 1 && tbomb <= 0) // bum
        bum(&bomb,bubak,pocet_bubaku,&hrac,pole,velikost_pole,pocet_zabitych_bubaku,pocet_tahu, pocet_bomb);
        
    if (p!='h' && p!='\n')
        vykresli(velikost_pole,pole,hrac,bomb,bubak,pocet_bubaku);

    // bubak snedl hrace
    for (int i = 0 ; i < pocet_bubaku ; i++ )
        if (bubak[i].e==1 && bubak[i].x == hrac.x && bubak[i].y == hrac.y)
            {
       // spocita pocet mrtvych bubaku
        for (int i = 0 ; i < pocet_bubaku ; i++ )
            if (bubak[i].e == 0) // neni, tedy je mrtvy
                pocet_zabitych_bubaku++;
    
                printf("  YOU ARE DEAD !! \n");
            printf("Statistics: \n\tturns: %d\n\tbombs: %d\n\tdead zombies: %d\n", pocet_tahu, pocet_bomb, pocet_zabitych_bubaku);
                exit(0);
            }            
    
    if (hrac.x == 0 && hrac.y == velikost_pole-1)
    {
        // spocita pocet mrtvych bubaku
        for (int i = 0 ; i < pocet_bubaku ; i++ )
            if (bubak[i].e == 0) // neni, tedy je mrtvy
                pocet_zabitych_bubaku++;
    
        printf("    YOU WIN !!! \n   CONGRATULATIONS !!!\n\n");
        printf("Statistics: \n\tturns: %d\n\tbombs: %d\n\tdead zombies: %d\n",pocet_tahu,pocet_bomb,pocet_zabitych_bubaku);
        exit(0);
    }
}

}
